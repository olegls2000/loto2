<#import "macro/base.ftl" as base>

<@base.page>
    <form action="/users/registration" method="post">
        <div>
            <label for="username">Username:</label>
            <input type="text" name="username"/>
        </div>
        <div>
            <label for="password">Password:</label>
            <input type="password" name="password"/>
        </div>
        <div>
            <label for="email">Email:</label>
            <input type="email" name="email"/>
        </div>
        <div>
            <label for="taxNumber">Tax Number:</label>
            <input type="number" name="taxNumber"/>
        </div>
        <div>
            <label for="firstName">First Name:</label>
            <input type="text" name="firstName"/>
        </div>
        <div>
            <label for="lastName">Last Name:</label>
            <input type="text" name="lastName"/>
        </div>

        <div>
            <input type="submit" value="Registration"/>
        </div>
    </form>
</@base.page>


