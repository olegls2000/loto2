<#import "macro/base.ftl" as base>

<@base.page>
    <form action="/login" method="post">
        <div class="form-group">
            <label for="username">Username:</label>
            <input type="text" name="username"/>


            <label for="password">Password:</label>
            <input type="password" name="password"/>

            <input type="submit" class="btn btn-success" value="Login"/>
        </div>
    </form>
</@base.page>