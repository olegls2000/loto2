package com.bta.loto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bta.loto.service.PlayService;

@Controller
@RequestMapping("play")
public class PlayController {

    @Autowired
    private PlayService playService;

    @GetMapping
    public ResponseEntity play() {
        playService.play();
        return ResponseEntity.ok("Play completed! Check your win!");
    }
}
