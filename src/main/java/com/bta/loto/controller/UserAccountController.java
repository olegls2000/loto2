package com.bta.loto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bta.loto.model.UserAccount;
import com.bta.loto.repository.UserAccountRepository;
import com.bta.loto.service.UserAccountService;

@Controller
@RequestMapping("users")
public class UserAccountController {

    @Autowired
    private UserAccountRepository userAccountRepository;

    @Autowired
    private UserAccountService userAccountService;

    //  localhost:8888/users -> all users
    @GetMapping
    public ResponseEntity<List<UserAccount>> getAllUsers() {
        final List<UserAccount> users = userAccountRepository.findAll();

        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    //       localhost:8888/users/ivan2 -> one user
    @GetMapping("{username}")
    public ResponseEntity<UserAccount> getUserByUsername(@PathVariable String username) {
        final List<UserAccount> users = userAccountRepository.findByUsername(username);

        return new ResponseEntity<>(users.get(0), HttpStatus.OK);
    }

    //       localhost:8888/users/registration -> register User
    @GetMapping("registration")
    public String registrationView(){
        return "registration";
    }

    @PostMapping("registration")
    public ResponseEntity register(@ModelAttribute UserAccount userAccount) {
        return userAccountService.registerUser(userAccount);
    }
}
