package com.bta.loto.repository;

import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.bta.loto.model.UserAccount;

@Repository
public class UserAccountRepository extends AbstractRepository<UserAccount> {
    public int save(UserAccount userAccount) {
        final String sql = "INSERT INTO user_account (id, username, password, email, tax_number, first_name, last_name, active, role)" +
                " values (nextval('user_account_seq'), " +
                ":username," +
                ":password," +
                ":email," +
                ":tax_number," +
                ":first_name," +
                ":last_name," +
                ":active," +
                ":role" +
                ")";

        MapSqlParameterSource map = new MapSqlParameterSource();
        map.addValue("username", userAccount.getUsername());
        map.addValue("password", userAccount.getPassword());
        map.addValue("email", userAccount.getEmail());
        map.addValue("tax_number", userAccount.getTaxNumber());
        map.addValue("first_name", userAccount.getFirstName());
        map.addValue("last_name", userAccount.getLastName());
        map.addValue("active", userAccount.isActive());
        map.addValue("role", userAccount.getRole());

        return namedParameterJdbcTemplate.update(sql, map);
    }

    @Override
    public int update(UserAccount entity) {
        //TODO HOmeWork!!!!
        return 0;
    }

    public List<UserAccount> findAll() {
        return jdbcTemplate.query("SELECT * FROM USER_ACCOUNT", getRowMapper());
    }

    public List<UserAccount> findByEmail(String email) {
        //TODO..
        return null;
    }

    public List<UserAccount> findByTaxNumber(Long taxNumber) {
        //TODO..
        return null;
    }

    public List<UserAccount> findByUsername(String username) {
        String query = "SELECT * FROM USER_ACCOUNT where username = :username";

        MapSqlParameterSource map = new MapSqlParameterSource();
        map.addValue("username", username);


        return namedParameterJdbcTemplate.query(query, map, getRowMapper());
    }

    private RowMapper<UserAccount> getRowMapper() {
        return (RowMapper) (resultSet, i) -> new UserAccount(
                resultSet.getLong("id"),
                resultSet.getString("username"),
                resultSet.getString("password"),
                resultSet.getString("email"),
                resultSet.getLong("tax_number"),
                resultSet.getString("first_name"),
                resultSet.getString("last_name"),
                resultSet.getBoolean("active"),
                resultSet.getString("role"));
    }
}
