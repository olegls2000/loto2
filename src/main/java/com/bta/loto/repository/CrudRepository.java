package com.bta.loto.repository;

import java.util.List;

import com.bta.loto.model.BaseEntity;

public interface CrudRepository<E extends BaseEntity> {
    int save(E entity);

    int update(E entity);

    List<E> findAll();
}
