package com.bta.loto.repository;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.bta.loto.model.PlayResult;

@Repository
public class PlayResultRepository extends AbstractRepository<PlayResult> {
    @Override
    public int save(PlayResult entity) {
        final String sql = "INSERT INTO result (id, number1, number2, number3, number4, number5, number6, " +
                "datetime) " +
                "values (NEXTVAL('result_seq'), " +
                ":number1," +
                ":number2," +
                ":number3," +
                ":number4," +
                ":number5," +
                ":number6," +
                ":datetime)";
        return namedParameterJdbcTemplate.update(sql, getMap(entity));
    }

    @Override
    public int update(PlayResult entity) {
        //TODO HOmeWork!!!!
        return 0;
    }

    @Override
    public List<PlayResult> findAll() {
        //TODO HOmeWork!!!!
        return null;
    }

    private MapSqlParameterSource getMap(PlayResult entity) {
        MapSqlParameterSource map = new MapSqlParameterSource();
        map.addValue("number1", entity.getNumber1());
        map.addValue("number2", entity.getNumber2());
        map.addValue("number3", entity.getNumber3());
        map.addValue("number4", entity.getNumber4());
        map.addValue("number5", entity.getNumber5());
        map.addValue("number6", entity.getNumber6());
        map.addValue("datetime", entity.getDateTime());
        return map;
    }
}
