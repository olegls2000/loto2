package com.bta.loto.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserAccount extends BaseEntity {
    @NotNull
    @NotEmpty
    @Size(max = 50)
    private String username;

    @NotNull
    @NotEmpty
    private String password;

    @Email
    private String email;

    @Max(999999)
    private Long taxNumber;
    private String firstName;
    private String lastName;

    private boolean active;

    private String role;

    public UserAccount() {
    }

    public UserAccount(String username, String password, String email, Long taxNumber,
                       String firstName, String lastName, boolean active, String role) {
        this(null, username, password, email, taxNumber, firstName, lastName, active, role);
    }

    public UserAccount(Long id, String username, String password, String email, Long taxNumber,
                       String firstName, String lastName, boolean active, String role) {
        super(id);
        this.username = username;
        this.password = password;
        this.email = email;
        this.taxNumber = taxNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.active = active;
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(Long taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
