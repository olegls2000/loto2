package com.bta.loto.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bta.loto.model.Bet;
import com.bta.loto.model.PlayResult;
import com.bta.loto.repository.BetRepository;
import com.bta.loto.repository.PlayResultRepository;

@Service
public class PlayService {
    private static final int COUNT = 6;
    private static final int MIN = 0;
    private static final int MAX = 48;

    @Autowired
    private PlayResultRepository playResultRepository;

    @Autowired
    private BetRepository betRepository;

    //6 from 48
    public void play() {
        final Set<Integer> results = new HashSet<>(10);
        while (results.size() <= COUNT) {
            results.add(getRandom());
        }

        PlayResult playResult = new PlayResult();
        playResult.setDateTime(LocalDateTime.now());
        final List<Integer> resultList = new ArrayList<>(results);
        playResult.setNumber1(resultList.get(0));
        playResult.setNumber2(resultList.get(1));
        playResult.setNumber3(resultList.get(2));
        playResult.setNumber4(resultList.get(3));
        playResult.setNumber5(resultList.get(4));
        playResult.setNumber6(resultList.get(5));
        playResultRepository.save(playResult);

        final List<Bet> activeBets = betRepository.findAllActive();
        for (Bet bet : activeBets) {
            evaluateBet(bet, playResult);
        }
    }

    private void evaluateBet(Bet bet, PlayResult result) {
        final List<Integer> collectionFromBet = getCollectionFromBet(bet);
        final List<Integer> collectionFromResult = getCollectionFromResult(result);
        int winNumbersCount = 0;
        for (int i = 0; i < COUNT; i++) {
            if (collectionFromBet.contains(collectionFromResult.get(i))) {
                winNumbersCount++;
            }
        }
        switch (winNumbersCount) {
            case 2:
                System.out.println("You win 200 EUROs!!!");
                break;
            case 3:
                System.out.println("You win 400 EUROs!!!");
                break;
            case 4:
                System.out.println("You win 1000 EUROs!!!");
                break;
            case 5:
                System.out.println("You win 5000 EUROs!!!");
                break;
            case 6:
                System.out.println("Jack POT! HURRRAAAAAA!!!!");
                break;
            default:
                System.out.println("You will win Next time!!!");
        }
        bet.setActive(false);
        betRepository.update(bet);
    }


    private int getRandom() {
        return (int) (Math.random() * MAX - MIN) + MIN;
    }

    private List<Integer> getCollectionFromBet(Bet bet) {
        return Arrays.asList(bet.getNumber1(),
                bet.getNumber2(),
                bet.getNumber3(),
                bet.getNumber4(),
                bet.getNumber5(),
                bet.getNumber6());
    }

    private List<Integer> getCollectionFromResult(PlayResult result) {
        return Arrays.asList(result.getNumber1(),
                result.getNumber2(),
                result.getNumber3(),
                result.getNumber4(),
                result.getNumber5(),
                result.getNumber6());
    }
}
