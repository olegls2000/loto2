package com.bta.loto.service;

import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;
import static org.springframework.http.HttpStatus.OK;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.bta.loto.model.UserAccount;
import com.bta.loto.repository.UserAccountRepository;

@Service
public class UserAccountService {
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private UserAccountRepository userAccountRepository;

    public ResponseEntity registerUser(UserAccount userAccount) {
        List<UserAccount> userAccounts = userAccountRepository.findByUsername(userAccount.getUsername());
        if (!userAccounts.isEmpty()) {
            return new ResponseEntity<>("User already registered", NOT_ACCEPTABLE);
        }

        userAccounts = userAccountRepository.findByEmail(userAccount.getEmail());
        if (!userAccounts.isEmpty()) {
            return new ResponseEntity<>("User with mentioned Email already registered", NOT_ACCEPTABLE);
        }

        userAccounts = userAccountRepository.findByTaxNumber(userAccount.getTaxNumber());
        if (!userAccounts.isEmpty()) {
            return new ResponseEntity<>("User with mentioned Tax Number already registered", NOT_ACCEPTABLE);
        }

        final String encodedPassword = passwordEncoder.encode(userAccount.getPassword());
        userAccount.setPassword(encodedPassword);
        userAccount.setActive(true);
        userAccount.setRole("USER");

        userAccountRepository.save(userAccount);

        return new ResponseEntity<>("User successfully registered", OK);
    }
}
